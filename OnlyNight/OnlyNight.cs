﻿using UnityEngine;
using UnityEngine.AzureSky;
using UnityEngine.SceneManagement;

namespace OnlyNight
{
    public class OnlyNight : Mod
    {
        public void Start()
        {
            Debug.Log("OnlyNight mod loaded.");
        }
        
        public void Update() {
            AzureSkyController controller = FindObjectOfType<AzureSkyController>();
            if (SceneManager.GetActiveScene().buildIndex == 1 &&
                !BedManager.BedTime() &&
                controller != null)
            {
                controller.timeOfDay.GotoTime(ComponentManager<BedManager>.Value.canSleepAfterTime + 0.1f);
                WorldManager.DayCounter++;
                Debug.Log("OnlyNight: Skipped the day.");
            }
        }

        public void OnModUnload()
        {
            Debug.Log("OnlyNight mod unloaded.");
        }
    }
}