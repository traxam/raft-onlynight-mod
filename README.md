﻿![banner image](./OnlyNight/banner.png)

# OnlyNight
*([view on RaftModding](https://www.raftmodding.com/mods/onlynight))*

This mod skips the day. That's it.

Don't check out [NoNight](https://raftmodding.com/mods/nonight).